# Copyright salsa-ci-team and others
# SPDX-License-Identifier: FSFAP
# Copying and distribution of this file, with or without modification, are
# permitted in any medium without royalty provided the copyright notice and
# this notice are preserved. This file is offered as-is, without any warranty.

.changes_images: &changes_images
  changes:
    - images/**/*
    - .images-*.yml

.should_build_images: &should_build_images
  only:
    <<: *changes_images

image config:
  stage: prerequisites
  image: debian
  script:
    - mkdir -p "${CI_PROJECT_DIR}/docker/configs"
    - touch "${CI_PROJECT_DIR}/docker/configs/should_build_images"
  artifacts:
    paths:
      - ${CI_PROJECT_DIR}/docker/configs/should_build_images
  <<: *should_build_images

.build_template: &build_template
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
    - cd ${CI_PROJECT_DIR}/images
  script:
    - NAME=$(echo ${CI_JOB_NAME} | awk -F':' '{ print $1 }')
    - RELEASE=$(echo ${CI_JOB_NAME} | awk -F':' '{ print $2 }')
    - STAGING_TAG=${CI_PIPELINE_ID}-
    - IMAGE=staging:${STAGING_TAG}${NAME}-${RELEASE}
    - eval "BASE_IMAGE=${BASE_IMAGE}"
    - |
        if [ -e "${CI_PROJECT_DIR}/docker/configs/should_build_images" ]; then
            docker build --pull -t ${CI_REGISTRY_IMAGE}/${IMAGE} \
                --build-arg VENDOR=${VENDOR} \
                --build-arg RELEASE=${RELEASE} \
                --build-arg BASE_IMAGE=${BASE_IMAGE} \
                -f dockerfiles/${NAME} .
        elif docker pull ${CI_REGISTRY_IMAGE}/${NAME}:${RELEASE}; then
            docker tag ${CI_REGISTRY_IMAGE}/${NAME}:${RELEASE} ${CI_REGISTRY_IMAGE}/${IMAGE}
        else
            docker pull registry.salsa.debian.org/salsa-ci-team/pipeline/${NAME}:${RELEASE}
            docker tag registry.salsa.debian.org/salsa-ci-team/pipeline/${NAME}:${RELEASE} ${CI_REGISTRY_IMAGE}/${IMAGE}
        fi
    - |
        docker push $CI_REGISTRY_IMAGE/$IMAGE
        case "$VENDOR" in
            debian)
                ALIASES=$(wget -O - http://deb.debian.org/debian/dists/${RELEASE}/Release | awk "/^Suite:/ { if (\$2 !~ /${RELEASE}/) print \$2 }")
                test "$ALIASES" != 'unstable' || ALIASES="${ALIASES} latest"
            ;;
            kali)
                test "$RELEASE" != "kali-dev" || ALIASES="${ALIASES} latest"
            ;;
        esac
        for alias in $ALIASES; do
            docker tag ${CI_REGISTRY_IMAGE}/${IMAGE} ${CI_REGISTRY_IMAGE}/staging:${STAGING_TAG}${NAME}-${alias}
            docker push ${CI_REGISTRY_IMAGE}/staging:${STAGING_TAG}${NAME}-${alias}
        done
    - |
        mkdir -p "${CI_PROJECT_DIR}/docker/configs"
        echo -n "${ALIASES}" > "${CI_PROJECT_DIR}/docker/configs/alias-${NAME}:${RELEASE}"
  artifacts:
    paths:
      - ${CI_PROJECT_DIR}/docker/configs/alias-*

deploy images:
  stage: deploy
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - docker login -u ${CI_REGISTRY_USER} -p ${CI_JOB_TOKEN} ${CI_REGISTRY}
  script:
    - |
        for conf in $(find "${CI_PROJECT_DIR}/docker/configs" -name alias-\* | sort); do
            NAME=${conf#*alias-}
            RELEASE=${NAME#*:}
            NAME=${NAME%:*}
            IMAGE=staging:${CI_PIPELINE_ID}-${NAME}-${RELEASE}
            docker pull ${CI_REGISTRY_IMAGE}/${IMAGE}
            ALIASES=$(cat "${conf}")
            echo "##### Pushing ${IMAGE} as ${RELEASE}${ALIASES:+ ${ALIASES}} in ${CI_REGISTRY_IMAGE}/${NAME} #####"
            for alias in ${RELEASE} ${ALIASES}; do
                docker tag ${CI_REGISTRY_IMAGE}/${IMAGE} ${CI_REGISTRY_IMAGE}/${NAME}:${alias}
                docker push ${CI_REGISTRY_IMAGE}/${NAME}:${alias}
            done
        done
  variables:
    GIT_STRATEGY: none
  <<: *should_build_images
  only:
    <<: *changes_images
    refs:
      - master

.build_base: &build_base
  <<: *build_template
  stage: images_base
  variables:
    BASE_IMAGE: $${VENDOR}:$${RELEASE}

.build_others: &build_others
  <<: *build_template
  stage: images_others
  variables:
    BASE_IMAGE: ${CI_REGISTRY_IMAGE}/staging:${CI_PIPELINE_ID}-base-$${RELEASE}
